package claude

import (
	"net/http"
)

// Client represents the Anthropic API client and its configuration.
type Client struct {
	httpClient *http.Client
	apiKey     string
	baseURL    string
}

// NewClient initializes a new Anthropic API client with the required headers.
func NewClient(apiKey string, opts ...ClientOptions) (*Client, error) {
	if apiKey == "" {
		return nil, ErrAnthropicApiKeyRequired
	}

	var opt clientOption
	for _, options := range opts {
		options(&opt)
	}

	client := &Client{
		httpClient: &http.Client{},
		apiKey:     apiKey,
		baseURL:    "https://api.anthropic.com",
	}

	if len(opt.baseURL) > 0 {
		client.baseURL = opt.baseURL
	}

	return client, nil
}

